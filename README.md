# ics-ans-role-ioc-dev

Ansible role to install several tools and conda enviroments for e3 development.

## Role Variables

```yaml
---
ioc_dev_e3_packages:
  - make
  - epics-base
  - tclx
  - require
  - compilers
  - streamdevice
  - e3-common

ioc_dev_e3_channels:
  - conda-e3-virtual

ioc_dev_cookiecutter_version: 1.7.2

```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioc-dev
```

## License

BSD 2-clause
