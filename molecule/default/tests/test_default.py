import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_alias_sript_exist(host):
    assert host.file("/etc/profile.d/e3-alias.sh").exists


def test_cookiecutter_exist(host):
    assert host.file("/opt/conda/envs/cookiecutter/bin/cookiecutter").exists


def test_cookiecutter_command(host):
    cmd = host.run("/opt/conda/envs/cookiecutter/bin/cookiecutter -h")
    assert "Cookiecutter is free" in cmd.stdout
